#pragma once
#include "Window.h"
#include "Tilemap.h"
#include "DXFont.h"
#include "cMutex.h"

class TestWindow : public Window
{
public:
	//public acces variables
	char CarSpeed = 0;
	char Direction = 0;
public:
	TestWindow()
		:
		Window()
	{}

	void SetWall(const PointI& p)
	{
		assert(pMap);
		pMap->SetBlock(p, true);
	}
	void SetCar(const PointF& c)
	{
		if (c != carPos)
		{
			carRotDes = (carPos - c).normalize() * 10.0f;
			carPos = c;
			pMap->SetBlock(carPos, true);
		}
	}
	bool isControlling() const
	{
		return carControlling;
	}
	void SetUpdateTime(float dt)
	{
		dtUpdate = dt;
	}
	std::string GetDebugString()
	{
		std::string res;
		muStr.Lock();
		res = strDebug;
		muStr.Unlock();
		return res;
	}
	std::string GetStateString()
	{
		std::string res;
		muStr.Lock();
		res = strState;
		muStr.Unlock();
		return res;
	}
	void SetDebugString(const std::string& s)
	{
		printf("debug: %s\n", s.c_str());
		muStr.Lock();
		strDebug = s;
		muStr.Unlock();
	}
	void SetStateString(const std::string& s)
	{
		printf("state: %s\n", s.c_str());
		muStr.Lock();
		strState = s;
		muStr.Unlock();
	}
protected:
	virtual void Start(LPDIRECT3DDEVICE9) override;
	virtual void DoFrame(LPDIRECT3DDEVICE9, Camera& cam, float dt) override;
	virtual void End() override;
	virtual LRESULT Message(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam) override;

private:
	Tilemap* pMap = nullptr;
	Cube3D* pCarCube = nullptr;
	const int mapRes = 1024;
	D3DXVECTOR3 camPos;
	PointF carRot;
	PointF carRotDes;
	PointF carPos;
	PointF carVelo;
	bool viewMode = false;
	bool bMousing = false;
	bool carControlling = false;
	float zoom = 20.0f;
	PointI mouseDown;
	Font* pFont = nullptr;

	Tile3D* pTileJoy = nullptr;
	Tile3D* pTileJoyCenter = nullptr;

	//Debugging
	std::string strState;
	std::string strDebug;
	cMutex muStr;
	float dtUpdate = 0.0f;
};