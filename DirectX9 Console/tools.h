#pragma once
#include <assert.h>
#include "Exception.h"


#define SafeDelete(p)	\
	delete (p);			\
	(p) = nullptr;		

#define SafeRelease(p)	\
if(p)					\
{						\
	(p)->Release();		\
	(p) = nullptr;			\
}

#define SafeDeleteArray(p)	\
	if(p){					\
	delete [] p;			\
	p = nullptr;}

#define clamp(a,mi,ma) max(mi,min(ma,a))