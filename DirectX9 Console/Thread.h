#pragma once
#define _WINSOCKAPI_   /* Prevent inclusion of winsock.h in windows.h !!! */
#define WINDOWS_LEAN_AND_MEAN
#include <windows.h>
#include <exception>

class Thread
{
public:
	Thread()
	{}
	virtual ~Thread()
	{
		End();
	}

	void Begin()
	{
		bIsRunning = true;
		hThread = CreateThread(nullptr,
			0,
			(LPTHREAD_START_ROUTINE)cThreadProc,
			this,
			0,
			(LPDWORD)&threadID);
		if (hThread == nullptr)
			throw std::exception("Thread::Begin() - Thread creation failed");
	}
	void End()
	{
		if (hThread != nullptr)
		{
			bIsRunning = false;
			WaitForSingleObject(hThread, INFINITE);
			CloseHandle(hThread);
			hThread = nullptr;
		}
	}
	bool IsRunning() const
	{
		return bIsRunning && !bError;
	}
	bool HasError() const
	{
		return bError;
	}
protected:
	virtual DWORD ThreadProc() = 0;
	void SetError()
	{
		bError = true;
	}
private:
	static DWORD WINAPI cThreadProc(Thread *pThis)
	{
		DWORD r = pThis->ThreadProc();
		pThis->bIsRunning = false;
		return r;
	}
private:
	HANDLE  hThread = nullptr;
	DWORD   threadID = 0;
	bool    bIsRunning = false;
	bool	bError = false;
};