#pragma once
#include "Input.h"
#include <stdlib.h>
#include <windows.h>
#include "Exception.h"
#include "Thread.h"

class ComHandle
{
protected:
	enum MSG
	{
		SPEED = 0x40,
		STEER = 0x80,
		SPECIAL = 0xC0,
		CTRL = 0x00,
		MASK_TYPE = 0xC0,
		MASK_DATA = 0x3F,
		MASK_SIGN = 0x20,
		MASK_UNDATA = 0x1F,

		DISCONNECT = 0xC1,

		CTRL_INIT = 0xC0,
		JOY_INIT = 0xC2,
		STOP = 0x3F
	};

public:
	ComHandle(HANDLE port)
		:
		h(port)
	{}
	bool SendByte(unsigned char b)
	{
		DWORD written;
		if( !WriteFile(h,&b,1,&written,NULL))
		{
			DWORD err = GetLastError();
			HRESULT res = HRESULT_FROM_WIN32(err);
			exHRESULT errmes = exHRESULT("ReadFile", res);
			printf("Com Handle WriteFile: %s\n", errmes.what());
		}
		return true;
	}
	// Blocking call
	bool ReadByte(unsigned char* dst)
	{
		unsigned char res;
		DWORD read;
		if (!ReadFile(h, &res, 1, &read, NULL))
		{
			DWORD err = GetLastError();
			HRESULT res = HRESULT_FROM_WIN32(err);
			if (err == ERROR_IO_PENDING)
				printf("pending error whilst reading!\n");
			else
			{
				exHRESULT errmes = exHRESULT("ReadFile", res);
				printf("Com Handle ReadFile: %s\n", errmes.what());
			}

			return false;
		}
		if(read == 1)
		{
			*dst = res;
			return true;
		}
		return false;
	}
	void SendDisconnect()
	{
		SendByte(MSG::DISCONNECT);
	}
	static unsigned char to6Bit(char bit)
	{
		if( bit < 0)
		{
			//keep sign
			bit = abs(bit);
			return (bit | (0x20)) & 0x3F;
		}
		else
		{
			return bit & 0x1f;
		}
	}
	static char from6Bit(char data)
	{
		data = data & MASK_DATA;
		if (data & MASK_SIGN)
		{
			//negative
			return 0 - (char)(data & MASK_UNDATA);
		}
		else
		{
			return data;
		}
	}
private:
	HANDLE h;
};