#include "Window.h"
#include "Exception.h"

#define WND_STYLE WS_OVERLAPPEDWINDOW
static Window* curWnd = nullptr;

Window::Window(){}

bool Window::Init(int wi, int hi, const std::wstring& name)
{
	clientArea.x = wi;
	clientArea.y = hi;
	className = name;
	try
	{
		/********************************************
		/* Register WNDCLASSEX **********************
		*********************************************/
		ZeroMemory(&wndClass, sizeof(wndClass));
		wndClass.cbSize = sizeof(wndClass);
		wndClass.lpfnWndProc = MsgProc;
		wndClass.hInstance = GetModuleHandle(NULL);
		wndClass.lpszClassName = name.c_str();
		wndClass.hbrBackground = (HBRUSH)COLOR_WINDOW;
		wndClass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
		wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
		wndClass.hIcon = NULL;
		wndClass.hIconSm = NULL;

		if (!RegisterClassEx(&wndClass))
		{
			HRESULT res = GetLastError();
			throw exHRESULT("RegisterClassEx", res);
		}

		int winWidth = GetSystemMetrics(SM_CXSCREEN);
		int winHeight = GetSystemMetrics(SM_CYSCREEN);

		//Adjust Window Rectangle
		RECT wndRect;
		wndRect.left = (winWidth - clientArea.x) / 2;
		wndRect.top = (winHeight - clientArea.y) / 2;
		wndRect.right = (winWidth + clientArea.x) / 2;
		wndRect.bottom = (winHeight + clientArea.y) / 2;
		int yStart = wndRect.top;
		int xStart = wndRect.left;

		AdjustWindowRect(&wndRect, WND_STYLE, 0);
		winWidth = wndRect.right - wndRect.left;
		winHeight = wndRect.bottom - wndRect.top;

		//Create the Window
		hWnd = CreateWindow(name.c_str(), name.c_str(), WND_STYLE,
			xStart, yStart, winWidth, winHeight, 0, 0, wndClass.hInstance, 0);

		if (!hWnd)
		{
			HRESULT res = GetLastError();
			throw exHRESULT("CreateWindow", res);
		}

		ShowWindow(hWnd, SW_SHOW);
		SetForegroundWindow(hWnd);
		SetFocus(hWnd);
	}
	catch (const std::exception& e)
	{
		printf("Error: %s\n", e.what());
		return false;
	}

	if (!gfx.Init(hWnd,clientArea.x,clientArea.y))
		return false;

	Start(gfx.GetDevice());
	timer.StartWatch();
	return true;
}

bool Window::Update()
{
	if (hWnd == 0)
		return false;

	MSG msg;
	ZeroMemory(&msg, sizeof(msg));

	curWnd = this;
	while (PeekMessage(&msg, hWnd, 0, 0, PM_REMOVE))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
	curWnd = nullptr;

	return true;
}
bool Window::Draw()
{
	float dt = timer.GetTimeSecond();
	timer.StartWatch();

	gfx.BeginFrame();
	DoFrame(gfx.GetDevice(), gfx.cam, dt);
	gfx.EndFrame();

	return true;
}

LRESULT WINAPI Window::MsgProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	if (curWnd)
	{
		switch (msg)
		{
		case WM_DESTROY:
			curWnd->Close();
			return 0;
			break;

		default:
			return curWnd->Message(hWnd, msg, wParam, lParam);
			break;
		}
	}
	return DefWindowProc(hWnd, msg, wParam, lParam);
}

void Window::Close()
{
	if (hWnd)
	{
		End();
		gfx.Close();

		DestroyWindow(hWnd);
		hWnd = 0;
	}

	if (wndClass.hInstance)
	{
		UnregisterClass(className.c_str(), wndClass.hInstance);
		wndClass.hInstance = 0;
	}
}

Window::~Window()
{
	Close();
}