#pragma once
#define _WINSOCKAPI_   /* Prevent inclusion of winsock.h in windows.h !!! */
#include <Windows.h>
#pragma comment(lib, "winmm.lib")

class cTimer
{
public:
	cTimer()
	{
		unsigned long long frequency;
		QueryPerformanceFrequency((LARGE_INTEGER*)&frequency);

		invFreqMili = 1.0f / (float)((double)frequency / 1000.0);
		watchStopped = true;
	}
	virtual ~cTimer()
	{

	}
	void StartWatch()
	{
		watchStopped = false;
		QueryPerformanceCounter((LARGE_INTEGER*)&startCount);
	}
	void StopWatch()
	{
		if (!watchStopped)
		{
			QueryPerformanceCounter((LARGE_INTEGER*)&currentCount);
			watchStopped = true;
		}
	}
	float GetTimeMilli() const
	{
		if (!watchStopped)
		{
			QueryPerformanceCounter((LARGE_INTEGER*)&currentCount);
			return (float)(currentCount - startCount) * invFreqMili;
		}
		else
		{
			return (float)(currentCount - startCount) * invFreqMili;
		}
	}
	float GetTimeSecond() const
	{
		return GetTimeMilli() / 1000.0f;
	}
	inline static unsigned long long SetTimestamp()
	{
		unsigned long long val;
		QueryPerformanceCounter((LARGE_INTEGER*)&val);
		return val;
	}
	float ConvertMilli(const unsigned long long& val1,const unsigned long long& val2) const
	{
		return (float)(val2 - val1) * invFreqMili;
	}
	float ConvertMilli(const unsigned long long& val1) const
	{
		return (float)(SetTimestamp() - val1) * invFreqMili;
	}
	void SetTime(float ms)
	{
		watchStopped = false;
		QueryPerformanceCounter((LARGE_INTEGER*)&currentCount);
		startCount = currentCount - (unsigned long long)(ms / invFreqMili);
	}
	unsigned long long GetStartStamp() const
	{
		if (watchStopped == false)
		{
			return startCount;
		}
		else
		{
			return 0;
		}
	}
private:
	float invFreqMili;
	bool watchStopped;
	unsigned long long currentCount;
	unsigned long long startCount;
};

