#pragma once
#include "Point.h"
#include <Windows.h>
#include <string>
#include "Graphic.h"
#include "cTimer.h"

class Window
{
public:
	Window();
	bool Init(int wi, int hi, const std::wstring& name);
	void Close();
	//returns false if it wants to be closed
	bool Update();
	bool Draw();
	~Window();

protected:
	virtual void Start(LPDIRECT3DDEVICE9){}
	virtual void DoFrame(LPDIRECT3DDEVICE9, Camera& cam,float dt) = 0;
	virtual void End(){}
	virtual LRESULT Message(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
	{
		return DefWindowProc(hWnd, msg, wParam, lParam);
	}
private:
	static LRESULT WINAPI MsgProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
private:
	HWND hWnd = 0;
	PointI clientArea;

	WNDCLASSEX wndClass;
	std::wstring className;

	Graphic gfx;

	cTimer timer;
};