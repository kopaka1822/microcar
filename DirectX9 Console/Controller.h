#pragma once
#include "ComHandle.h"
#include "cMutex.h"

class Controller :  public ComHandle
{
	enum CARMSG
	{
		HEADPOS = 0x80,

		XHEAD = 0,
		YHEAD = 0x40,

		LOW = 0,
		HIGH = 0x20,
		MASKDATA = 0x1F
	};

	class ComReader : public Thread
	{
	public:
		ComReader(HANDLE h)
			:
			h(h),
			lastpos(512, 512),
			newpos(512, 512)
		{}
		virtual DWORD ThreadProc() override
		{
			while (1)
			{
				if (!IsRunning())
					return 0;

				char data;
				DWORD read;
				if (!ReadFile(h, &data, 1, &read, NULL))
				{
					if (GetLastError() == ERROR_IO_PENDING)
						printf("pending error whilst reading!\n");

					SetError();
					return -1;
				}
				
				if (data & CARMSG::HEADPOS)
				{
					// position
					unsigned short newVal;
					if (data & CARMSG::YHEAD)
					{
						newVal = lastpos.y;
					}
					else
					{
						newVal = lastpos.x;
					}

					if (data & CARMSG::HIGH)
					{
						newVal = ((unsigned short)newVal & 0x1F)
							| ((unsigned short)(data & CARMSG::MASKDATA) << 5);
					}
					else
					{
						newVal = (unsigned char)(data & CARMSG::MASKDATA);
					}

					if (data & CARMSG::YHEAD)
					{
						lastpos.y = newVal;
						if (data & CARMSG::HIGH)
						{
							muData.Lock();
							newpos = lastpos;
							muData.Unlock();
						}
					}
					else
					{
						lastpos.x = newVal;
					}
				}
				//muData.Lock();
				//this->data.push_back(data);
				//muData.Unlock();
			}
		}
		PointI GetPos() const
		{
			PointI res;
			muData.Lock();
			res = newpos;
			muData.Unlock();
			return res;
		}
	private:
		std::vector< char > data;
		cMutex muData;
		HANDLE h;
		PointI lastpos;
		PointI newpos;
	};
public:
	Controller(HANDLE port)
		:
		ComHandle(port),
		isConnected(false)
		,reader(port)
	{
		//send init code
		if (!SendByte(MSG::CTRL_INIT))
		{
			printf("\n: could not send init code 0xC0\n");
			return;
		}
		printf("\ninit code 0xC0 sent\n");
		unsigned char tst = 0;
		for(int i = 0; i < 10; i++)
		{
			if (ReadByte(&tst))
			{
				if (tst == MSG::CTRL_INIT)
					break;

				printf("wrong response %X\n", tst);
				i--;
			}

			printf(".");
			Sleep(1000);
		}
		if (tst == MSG::CTRL_INIT)
		{
			isConnected = true;
			reader.Begin();
		}
	}
	~Controller()
	{
		reader.End();
	}
	bool Connected() const
	{
		return isConnected;
	}
	bool SetSpeed(char speed)
	{
		this->speed = speed;
		return SendByte(MSG::SPEED | to6Bit(speed));
	}
	char GetSpeed() const
	{
		return speed;
	}
	char GetDir() const
	{
		return dir;
	}
	// > 0 left  // < 0 right
	bool SetDirection(char dir)
	{
		this->dir = dir;
		return SendByte(MSG::STEER | to6Bit(dir));
	}
	void SendStop()
	{
		if (isConnected)
		{
			if (SendByte(MSG::SPECIAL | MSG::STOP))
			{
				isConnected = false;
			} 
		}
	}
	PointI GetPos()
	{
		return reader.GetPos();
	}
	bool hasError() const
	{
		return reader.HasError();
	}
private:
	char speed;
	char dir;
	bool isConnected;
	ComReader reader;
};