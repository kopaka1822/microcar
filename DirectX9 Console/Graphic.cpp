#include "Graphic.h"
#include "Exception.h"
#include "tools.h"

Graphic::Graphic()
{}

Graphic::~Graphic()
{
	Close();
}

bool Graphic::Init(HWND hWnd, int wi, int hi)
{
	if (bInit)
		return false;

	HRESULT res = S_OK;
	try
	{
		pD3D9 = Direct3DCreate9(D3D_SDK_VERSION);
		if (!pD3D9)
			throw std::exception("Direct3DCreate9 failed");

		D3DDISPLAYMODE d3ddm;
		res = pD3D9->GetAdapterDisplayMode(D3DADAPTER_DEFAULT, &d3ddm);
		if (FAILED(res))
			throw exHRESULT("pD3D9->GetAdapterDisplayMode", res);

		ZeroMemory(&d3dpp, sizeof(d3dpp));

		d3dpp.Windowed = TRUE;
		d3dpp.SwapEffect = D3DSWAPEFFECT_DISCARD;
		d3dpp.BackBufferFormat = d3ddm.Format;
		d3dpp.EnableAutoDepthStencil = TRUE;
		d3dpp.AutoDepthStencilFormat = D3DFMT_D16;
		d3dpp.BackBufferWidth = wi;
		d3dpp.BackBufferHeight = hi;
		d3dpp.PresentationInterval = D3DPRESENT_INTERVAL_ONE;

		res = pD3D9->CreateDevice(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, hWnd,
			D3DCREATE_HARDWARE_VERTEXPROCESSING,
			&d3dpp, &pDevice);
		if (FAILED(res))
		{
			printf("CreateDevice with HARDWARE_VERTEXPROCESSING failed\n");
			printf("trying SOFTWARE_VERTEXPROCESSING...\n");

			res = pD3D9->CreateDevice(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, hWnd,
				D3DCREATE_SOFTWARE_VERTEXPROCESSING,
				&d3dpp, &pDevice);
			if (FAILED(res))
				throw exHRESULT("LPDIRECT3D9->CreateDevice", res);
		}

		cam.SetPerspective(2.0f, float(wi) / float(hi), 0.001f, 1000.0f);
		cam.SetLookAt(Point3(-5.0f, 0.0f, 0.0f), Point3(0.0f, 0.0f, 0.0f));

		pDevice->SetRenderState(D3DRS_LIGHTING, FALSE);
		pDevice->SetRenderState(D3DRS_ZENABLE, TRUE);

		ApplyCamera();
	}
	catch (const std::exception& e)
	{
		printf("Error: %s\n", e.what());
		return false;
	}
	return true;
}

void Graphic::ApplyCamera()
{
	pDevice->SetTransform(D3DTS_PROJECTION, cam.GetPerspective());
	pDevice->SetTransform(D3DTS_VIEW, cam.GetLookAt());
}

void Graphic::BeginFrame()
{
	pDevice->Clear(0, nullptr, D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER,
		D3DCOLOR_COLORVALUE(0.0f, 0.0f, 0.0f, 1.0f), 1.0f, 0);

	pDevice->BeginScene();
}

void Graphic::EndFrame()
{
	pDevice->EndScene();

	HRESULT res = pDevice->Present(0, 0, 0, 0);
	if (FAILED(res))
	{
		printf("LPDIRECT3DDEVICE9->Present failed!\n");
	}
}

void Graphic::Close()
{
	if (!bInit)
		return;

	bInit = false;
	SafeRelease(pDevice);
	SafeRelease(pD3D9);
}