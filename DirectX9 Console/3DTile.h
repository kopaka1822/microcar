#pragma once
#include "Graphic.h"
#include "tools.h"

class Tile3D
{
	struct Vertex
	{
		float x, y, z;
		DWORD color;

		enum FVF
		{
			FVF_Flags = D3DFVF_XYZ | D3DFVF_DIFFUSE
		};
	};

public:
	Tile3D(LPDIRECT3DDEVICE9 pDevice, DWORD col)
	{
		Vertex points[] = 
		{
			{ 0.0f, 0.0f, 1.0f, col },
			{ 1.0f, 0.0f, 1.0f, col },
			{ 0.0f, 0.0f, 0.0f, col },
			{ 1.0f, 0.0f, 0.0f, col }
		};

		pDevice->CreateVertexBuffer(4 * sizeof(Vertex),
			D3DUSAGE_WRITEONLY, Vertex::FVF_Flags,
			D3DPOOL_DEFAULT,
			&pVertexBuffer,
			NULL);

		void* pVertices;
		pVertexBuffer->Lock(0, sizeof(points), (void**)&pVertices, 0);
		memcpy(pVertices, points, sizeof(points));
		pVertexBuffer->Unlock();
	}
	void Draw(LPDIRECT3DDEVICE9 pDevice)
	{
		pDevice->SetStreamSource(0, pVertexBuffer, 0, sizeof(Vertex));
		pDevice->SetFVF(Vertex::FVF_Flags);

		pDevice->DrawPrimitive(D3DPT_TRIANGLESTRIP, 0, 2);
	}
	void Dispose()
	{
		SafeRelease(pVertexBuffer);
	}
	~Tile3D()
	{
		Dispose();
	}

private:
	LPDIRECT3DVERTEXBUFFER9 pVertexBuffer = NULL;
};