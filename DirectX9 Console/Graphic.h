#pragma once
#define WIN32_LEAN_AND_MEAN

#include <windows.h>
#include <mmsystem.h>
#include <d3d9.h>
#include <d3dx9.h>
#include <d3dx9math.h>
#pragma comment(lib,"d3d9.lib" )
#pragma comment(lib, "D3dx9.lib")
#include "Point.h"
#include "Camera.h"

class Graphic
{
public:
	Camera cam;
public:
	Graphic();
	~Graphic();

	bool Init(HWND hWnd, int wi, int hi);
	void Close();
	void ApplyCamera();
	void BeginFrame();
	void EndFrame();
	LPDIRECT3DDEVICE9 GetDevice()
	{
		return pDevice;
	}
private:
	bool bInit = false;
	LPDIRECT3D9 pD3D9 = nullptr;
	LPDIRECT3DDEVICE9 pDevice = nullptr;
	D3DPRESENT_PARAMETERS d3dpp;
};