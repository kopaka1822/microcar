#include "TestWindow.h"
#include "Input.h"
#include "Exception.h"
#include "Controller.h"
#include "Joystick.h"
#include "Timer.h"

#define SAVEFILE "config.dat"
int DefPortCar = 0;
int DefPortJoy = 0;

enum STATUS
{
	S_EXIT,
	S_INVALID,

	S_PORTTEST,
	S_PORTCONNECT,
	S_PORTCONJOY,
	S_MENU,
	S_JOYTEST,
	S_DXTEST
};

bool GetComHandle(HANDLE& h, int port);

STATUS MainLoop(Controller& car, Joystick* jst)
{
	Timer t;
	TestWindow wnd;
	wnd.Init(800, 800, L"test");

	STATUS r = S_INVALID;
	bool bChange = true;
	bool panic = false;

	float dtUpdate = 0.0f;

	class DrawThread : public Thread
	{
	public:
		DrawThread(TestWindow& t)
			:
			tst(t)
		{}
		virtual DWORD ThreadProc() override
		{
			try
			{
				while (IsRunning())
				{
					tst.Draw();
				}
			}
			catch (const std::exception& e)
			{
				printf("ERROR draw thread: %s\n", e.what());
				SetError();
				return -1;
			}
			catch (...)
			{
				SetError();
				return -1;
			}
			
			return 0;
		}
	private:
		TestWindow& tst;
	};

	DrawThread drawThread(wnd);


	drawThread.Begin();
	while (!panic)
	{
		wnd.SetStateString("begin");
		if (HasKey())
		{
			if (GetKey() == 'q')
				panic = true;
		}

		wnd.SetStateString("window redraw");
		wnd.SetUpdateTime(dtUpdate);
		wnd.Update();

		t.StartWatch();
		if (wnd.isControlling())
		{
			wnd.SetStateString("upd window");
			if (car.GetSpeed() != wnd.CarSpeed)
			{
				wnd.SetDebugString("wnd sending speed");
				car.SetSpeed(wnd.CarSpeed);
			}
				
			if (car.GetDir() != wnd.Direction)
			{
				wnd.SetDebugString("wnd sending dir");
				car.SetDirection(wnd.Direction);
			}
		}
		else
		{
			if (jst)
			{
				wnd.SetStateString("upd jst");
				wnd.CarSpeed = jst->GetSpeed();
				wnd.Direction = jst->GetDir();

				bChange = jst->Update();

				if (bChange)
				{
					wnd.SetStateString("jst sending changes");
					if (car.GetSpeed() != jst->GetSpeed())
						car.SetSpeed(jst->GetSpeed());

					if (car.GetDir() != jst->GetDir())
						car.SetDirection(jst->GetDir());

					panic = jst->GetPanicButton();
					if (panic)
					{
						printf("joystick send panic!\n");
						wnd.SetStateString("stopping car\n");
						car.SendStop();
						break;
					}
				}
			}
			else
			{
				//no jst
				if (car.GetSpeed() != 0)
				{
					wnd.SetStateString("wnd sending speed");
					car.SetSpeed(0);
				}
				if (car.GetDir() != 0)
				{
					wnd.SetStateString("wnd sending dir");
					car.SetDirection(0);
				}
			}
		}
		if (panic)
		{
			wnd.SetStateString("closing..\n");
			//panic was triggered by pc
			printf("force disconnect...\n");
			car.SendStop();
			printf("car disconnected..\n");
			if(jst) jst->SendDisconnect();
			printf("joystick disconnected..\n");
			break;
		}

		
		wnd.SetStateString("reading from car");
		PointI position = car.GetPos();

		wnd.SetCar(position);
		
		if (car.hasError())
		{
			wnd.SetDebugString("Car Reading Error");
			panic = true;
		}
		dtUpdate = t.GetTimeMilli();
	}
	drawThread.End();
	r = S_MENU;
	wnd.Close();
	return r;
}

STATUS ConnectBlue(bool remoteJoystick)
{
	STATUS r = S_MENU;

	Joystick* pJst = NULL;
	HANDLE hj = NULL;
	//connect joystick
	if (remoteJoystick)
	{
		printf("connecting to joystick\n");
		if (!GetComHandle(hj, DefPortJoy))
		{
			return r;
		}

		pJst = new Joystick(hj);

		if (!pJst->Connected())
		{
			printf("\nconnection timed out\n");
			CloseHandle(hj);
			return r;
		}
		printf("\nconnection to joystick established!\n");
		Sleep(5000);
	}

	//connect car
	printf("connecting car...\n");
	HANDLE h;
	if (!GetComHandle(h,DefPortCar))
	{
		if (pJst)
			pJst->SendDisconnect();
		CloseHandle(hj);
		return r;
	}

	printf("connecting");
	Controller ctrl(h);

	if (!ctrl.Connected())
	{
		printf("\nconnection timed out\n");
		CloseHandle(h);
		if (pJst)
			pJst->SendDisconnect();
		CloseHandle(hj);
		return r;
	}
	printf("\nconnection to car established!\n");

	r = MainLoop(ctrl, pJst);

	CloseHandle(h);
	if (hj)
		CloseHandle(hj);
	delete pJst;
	pJst = NULL;

	printf("disconnected\n");
	return r;
}

void TestJoystick()
{
	STATUS r = S_MENU;
	HANDLE h;
	printf("connecting to joystick\n");
	if (!GetComHandle(h,DefPortJoy))
	{
		return;
	}

	Joystick joy(h);

	if (!joy.Connected())
	{
		printf("\nconnection timed out\n");
		CloseHandle(h);
		return;
	}
	printf("\nconnection to joystick established!\n");

	int k = 0;
	int direction = 0;
	int speed = 0;
	bool bChange = true;
	bool panic = true;

	while (k != 'q')
	{
		if (HasKey())
		{
			k = GetKey();
		}
		if (bChange)
		{
			Clear();
			printf("speed: %d\n", joy.GetSpeed());
			printf("direction: %d\n", joy.GetDir());
			if (joy.GetPanicButton())
			{
				printf("panic pressed!\n");
				panic = true;
				break;
			}
			bChange = false;
		}

		bChange = joy.Update();
	}

	printf("disconnecting...\n");
	if (!panic)
		joy.SendDisconnect();
	CloseHandle(h);
}

void DXTest()
{
	TestWindow tst;
	tst.Init(800, 800, L"DX9 test");

	while (tst.Update())
	{
		tst.Draw();
	}
}

STATUS Menu();
void TestPorts();

int main()
{
	//load settings
	FILE* pFile = nullptr;
	errno_t err = fopen_s(&pFile, SAVEFILE, "rb");
	if (!err)
	{
		fread(&DefPortCar, sizeof(int), 1, pFile);
		fread(&DefPortJoy, sizeof(int), 1, pFile);
		fclose(pFile);
	}
	else
	{
		printf("no save file found, please set default ports\n");
		printf("Car: ");
		DefPortCar = GetInt();
		printf("Joystick: ");
		DefPortJoy = GetInt();

		//save file
		err = fopen_s(&pFile, SAVEFILE, "wb");
		if (!err)
		{
			fwrite(&DefPortCar, sizeof(int), 1, pFile);
			fwrite(&DefPortJoy, sizeof(int), 1, pFile);
			fclose(pFile);
		}
	}

	STATUS r = S_MENU;

	while (r != S_EXIT)
	{
		Clear();
		switch (r)
		{
		case S_MENU:
			r = Menu();
			break;
		case S_PORTTEST:
			TestPorts();
			r = S_MENU;
			break;
		case S_PORTCONNECT:
			r = ConnectBlue(false);
			Pause();
			break;
		case S_PORTCONJOY:
			r = ConnectBlue(true);
			Pause();
			break;
		case S_JOYTEST:
			TestJoystick();
			r = S_MENU;
			Pause();
			break;
		case S_DXTEST:
			DXTest();
			r = S_MENU;
			break;
		default:
			printf("wrong state\n");
			Pause();
			return -1;
		}
	}
	return 0;
}

STATUS Menu()
{
	printf("Menu\n\n");
	printf("\t1. test ports\n");
	printf("\t2. connect to port without joystick\n");
	printf("\t3. connect to port\n");
	printf("\t4. test joystick\n");
	printf("\t5. test DX9\n");
	printf("\tq. quit\n");

	STATUS r = S_INVALID;

	while (r == S_INVALID)
	{
		switch (GetKey())
		{
		case '1':
			r = S_PORTTEST;
			break;
		case '2':
			r = S_PORTCONNECT;
			break;
		case '3':
			r = S_PORTCONJOY;
			break;
		case '4':
			r = S_JOYTEST;
			break;
		case '5':
			r = S_DXTEST;
			break;
		case 'q':
			r = S_EXIT;
			break;
		}
	}

	return r;
}
void TestPorts()
{
	printf("Testing Com Ports...\n");

	wchar_t buffer[20];

	for (int i = 0; i < 40; i++)
	{
		wsprintf(buffer, L"\\\\.\\COM%d", i);
		HANDLE h = ::CreateFile(buffer, GENERIC_READ | GENERIC_WRITE, 0, 0, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, 0);

		if (h != INVALID_HANDLE_VALUE)
		{
			wprintf(L"Port %s okay!\n", buffer);
			CloseHandle(h);
		}
	}

	Pause();
}
bool GetComHandle(HANDLE& h, int port)
{
	wchar_t buffer[20];
	wsprintf(buffer, L"\\\\.\\COM%d", port);
	wprintf(L"\nConnecting to %s\n", buffer);

	h = ::CreateFile(buffer, GENERIC_READ | GENERIC_WRITE, 0, 0, OPEN_EXISTING, 
		FILE_ATTRIBUTE_NORMAL, 0);
	if (h == INVALID_HANDLE_VALUE)
	{
		DWORD res = HRESULT_FROM_WIN32(GetLastError());
		exHRESULT e = exHRESULT("create file: ", res);
		wprintf(L"%s", buffer);
		printf("%s\n", e.what());
		return false;
	}
	printf("port found\n");

	printf("get device state...\n");

	DCB dcb;
	memset(&dcb, 0, sizeof(dcb));
	dcb.DCBlength = sizeof(dcb);
	if (GetCommState(h, &dcb) == 0)
	{
		printf("error getting device state\n");
		CloseHandle(h);
		return false;
	}
	printf("setting baud rate etc...\n");

	dcb.BaudRate = CBR_9600;
	dcb.ByteSize = 8;
	dcb.StopBits = ONESTOPBIT;
	dcb.Parity = NOPARITY;

	if (SetCommState(h, &dcb) == 0)
	{
		printf("error setting device parameters\n");
		CloseHandle(h);
		return false;
	}
	return true;
}
