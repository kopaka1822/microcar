#pragma once
#include <windows.h>
#include <mmsystem.h>
#include <d3d9.h>
#include <d3dx9.h>
#include <d3dx9math.h>
#pragma comment(lib,"d3d9.lib" )
#pragma comment(lib, "D3dx9.lib")
#include <math.h>

using Point3 = D3DXVECTOR3;

class Camera
{
public:
	Camera()
	{}
	void SetPerspective(float fovy, float aspect, float zn, float zf)
	{
		D3DXMatrixPerspectiveFovLH(&projection, fovy, aspect, zn, zf);
	}
	const D3DXMATRIX* GetPerspective() const
	{
		return &projection;
	}
	D3DXMATRIX GetPerpectiveUp() const
	{
		D3DXMATRIX m;
		D3DXMatrixPerspectiveFovLH(&m, 2.0f * tanf( 400.0f ) , 1.0f, 0.01f, 100.0f);
		return m;
	}
	D3DXMATRIX GetLookUp() const
	{
		D3DXMATRIX m;
		D3DXMatrixLookAtLH(&m, &Point3(0.0f, 1.0f, 0.0f), &Point3(0.0f, 0.0f, 0.0f),
			&Point3(0.0f, 0.0f, 1.0f));
		return m;
	}
	void SetLookAt(const Point3& pos, const Point3& lookat,
		const Point3& up = Point3(0.0f, 1.0f, 0.0f))
	{
		D3DXMatrixLookAtLH(&look, &pos,
			&lookat, &up);
	}
	const D3DXMATRIX* GetLookAt() const
	{
		return &look;
	}
private:
	D3DXMATRIX projection;
	D3DXMATRIX look;
};