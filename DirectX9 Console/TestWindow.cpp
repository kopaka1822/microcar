#include "TestWindow.h"
#include "tools.h"
#include "3DCube.h"

void TestWindow::Start(LPDIRECT3DDEVICE9 pDevice)
{
	pMap = new Tilemap(pDevice,mapRes);
	pCarCube = new Cube3D(pDevice, D3DCOLOR_XRGB(255,0,0));

	carPos = PointF(mapRes / 2, mapRes / 2);

	carRot = PointF(10.0f,0.0f);
	carRotDes = carRot;

	pFont = new Font(20, pDevice);
	pFont->SetColor(D3DCOLOR_XRGB(255, 255, 255));

	pTileJoy = new Tile3D(pDevice, D3DCOLOR_XRGB(0, 0, 255));
	pTileJoyCenter = new Tile3D(pDevice, D3DCOLOR_XRGB(255, 255, 255));

	pMap->SetBlock(carPos, true);
}

void TestWindow::DoFrame(LPDIRECT3DDEVICE9 pDevice, Camera& cam, float dt)
{
	if (!pMap)
		return;

	D3DXVECTOR3 vUp;

	if (viewMode)
	{
		if (carRot != carRotDes && !(bMousing && !carControlling))
		{
			carRot = (carRot * 74.0f + carRotDes) / 75.0f;
		}
		camPos = D3DXVECTOR3(carRot.x + carPos.x, zoom / 2.0f, carRot.y + carPos.y);
		vUp = D3DXVECTOR3(0.0f, 1.0f, 0.0f);
	}
	else
	{
		camPos = D3DXVECTOR3(carPos.x, zoom, carPos.y);
		vUp = D3DXVECTOR3(1.0f, 0.0f, 0.0f);
		carRot = carRotDes;
	}


	cam.SetLookAt(camPos, D3DXVECTOR3(carPos.x, 0.0f, carPos.y), vUp);

	pDevice->SetTransform(D3DTS_PROJECTION, cam.GetPerspective());
	pDevice->SetTransform(D3DTS_VIEW, cam.GetLookAt());


	D3DXMATRIX matTrans;

	pMap->Draw(pDevice, nullptr, carPos);

	//pMap->SetBlock(carPos, true);

	D3DXMatrixTranslation(&matTrans, carPos.x, 0.0f, carPos.y);
	
	pDevice->SetTransform(D3DTS_WORLD, &matTrans);
	pCarCube->Draw(pDevice);

	////draw Joystick
	//D3DXMatrixIdentity(&matTrans);
	////pDevice->SetTransform(D3DTS_PROJECTION, &matTrans);
	////cam.SetLookAt(Point3(0.0f, 1.0f, 0.0f), Point3(0.0f, 0.0f, 0.0f), Point3(0.0f, 0.0f, 1.0f));
	//pDevice->SetTransform(D3DTS_VIEW, &cam.GetPerpectiveUp());
	//pDevice->SetTransform(D3DTS_VIEW, &cam.GetLookUp());
	//pDevice->SetTransform(D3DTS_WORLD, &matTrans);

	//pTileJoy->Draw(pDevice);

	// Draw Debug info
	pFont->Text(std::string("Speed: ") + std::to_string(CarSpeed), { 10, 10 });
	pFont->Text(std::string("Direction: ") + std::to_string(Direction), { 10, 30 });

	std::string str = GetStateString();
	if (str.length())
		pFont->Text(str, { 10, 50 });
	
	str = GetDebugString();
	if (str.length())
		pFont->Text(str, { 10, 70 });

	pFont->Text(std::string("pos: ( ") + std::to_string((long)carPos.x)
		+ std::string(" | ") + std::to_string((long)carPos.y) + std::string(" )"),
		{ 10, 90 });

	if (carControlling)
	{
		pFont->Text("Controlling Car!", {10,110});
	}

	pFont->Text(std::string("updtime: ") + std::to_string(dtUpdate), { 10, 130 });
}

LRESULT TestWindow::Message(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	static PointI ptCurrentMousePosit;
	static PointI ptLastMousePosit;

	switch (msg)
	{
	case WM_LBUTTONDOWN:
	{
		mouseDown.x = LOWORD(lParam);
		mouseDown.y = HIWORD(lParam);
		ptLastMousePosit = mouseDown;
		bMousing = true;
	}
	break;

	case WM_LBUTTONUP:
	{
		bMousing = false;
		CarSpeed = 0;
		Direction = 0;
	}
	break;

	case WM_KEYDOWN:
		switch (wParam)
		{
		case 'W':
			SetCar(carPos + PointF(1.0f,0.0f));
			break;
		case 'S':
			SetCar(carPos - PointF(1.0f, 0.0f));
			break;
		case 'A':
			SetCar(carPos + PointF(0.0f,1.0f));
			break;
		case 'D':
			SetCar(carPos - PointF(0.0f, 1.0f));
			break;
		case 'E':
			SetCar(carPos + PointF(1.0f, -1.0f));
			break;
		case 'Q':
			SetCar(carPos + PointF(1.0f, 1.0f));
			break;
		case 'C':
			carControlling = !carControlling;
			break;
		case '1':
			viewMode = !viewMode;
			break;
		}
		break;

	case WM_MOUSEMOVE:
	{
		PointI cur;

		cur.x = LOWORD(lParam);
		cur.y = HIWORD(lParam); 
		ptCurrentMousePosit = cur;

		if (bMousing)
		{
			

			if (carControlling)
			{
				int dx = mouseDown.x - cur.x;
				int dy = mouseDown.y - cur.y;


				CarSpeed = clamp((dy / 4), -20, 20);
				Direction = clamp((dx / 4), -20, 20);
			}
			else
			{
				carRot = carRot.rot((ptCurrentMousePosit.x - ptLastMousePosit.x) * 0.01f);
			}

			ptLastMousePosit = ptCurrentMousePosit;
		}
	}
	break;
	case WM_MOUSEWHEEL:
		if (GET_WHEEL_DELTA_WPARAM(wParam) < 0)
		{
			zoom = min(zoom * 1.1f, 40.0f);
		}
		else
		{
			zoom = max(zoom * 0.9f, 10.0f);
		}
	break;
	default:
		return DefWindowProc(hWnd, msg, wParam, lParam);
	}
	return DefWindowProc(hWnd, msg, wParam, lParam);
}

void TestWindow::End()
{
	SafeDelete(pMap);
	SafeDelete(pCarCube);
	SafeDelete(pFont);
	SafeDelete(pTileJoy);
	SafeDelete(pTileJoyCenter);
}