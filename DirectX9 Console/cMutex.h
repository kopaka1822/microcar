#pragma once
#define _WINSOCKAPI_   /* Prevent inclusion of winsock.h in windows.h !!! */
#define WINDOWS_LEAN_AND_MEAN
#include <windows.h>
#include <exception>

class cMutex
{
	HANDLE hMutex = nullptr;
public:
	cMutex()
	{
		hMutex = CreateMutex(nullptr, false, nullptr);
		if (hMutex == nullptr)
			throw std::exception("Mutex creation failed");
	}
	cMutex(const cMutex&) = delete;
	cMutex(cMutex&& move)
	{
		Swap(move);
	}
	virtual ~cMutex()
	{
		if (hMutex != nullptr)
		{
			CloseHandle(hMutex);
			hMutex = nullptr;
		}
	}

	void Lock() const
	{
		WaitForSingleObject(hMutex, INFINITE);
	}
	void Unlock() const
	{
		ReleaseMutex(hMutex);
	}
private:
	void Swap(cMutex& sw)
	{
		HANDLE tmp = hMutex;
		hMutex = sw.hMutex;
		sw.hMutex = tmp;
	}
};