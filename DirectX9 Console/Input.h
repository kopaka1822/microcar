#pragma once
#include <stdio.h>
#include <string>
#include <conio.h>

std::string GetString()
{
	char buffer[512];
	gets_s(buffer);
	return std::string(buffer);
}

std::wstring GetWString()
{
	std::string s = GetString();
	return std::wstring(s.begin(),s.end());
}

bool HasKey()
{
	return (_kbhit() != 0);
}

char GetKey()
{
	while (!_kbhit()){ Sleep(100); } //thread friendly
	return _getch();
}

void Clear()
{
	system("cls");
}

void Pause()
{
	printf("\npress any key...\n");
	GetKey();
}

int GetInt()
{
	std::string s = GetString();
	return atoi(s.c_str());
}