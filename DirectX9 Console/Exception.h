#pragma once

#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#include <exception>
#include <string>

class exHRESULT : public std::exception
{
public:
	exHRESULT(std::string function, HRESULT res)
		:
		res(res)
	{
		errmes = function + std::string(" failed with HRESULT: ");
		char buffer[50];
		sprintf_s(buffer, "%x", res);
		errmes += buffer;

		LPTSTR errorText = NULL;

		FormatMessage(
			// use system message tables to retrieve error text
			FORMAT_MESSAGE_FROM_SYSTEM
			// allocate buffer on local heap for error text
			| FORMAT_MESSAGE_ALLOCATE_BUFFER
			// Important! will fail otherwise, since we're not 
			// (and CANNOT) pass insertion parameters
			| FORMAT_MESSAGE_IGNORE_INSERTS,
			NULL,    // unused with FORMAT_MESSAGE_FROM_SYSTEM
			res,
			MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
			(LPTSTR)&errorText,  // output 
			0, // minimum size for output buffer
			NULL);   // arguments - see note 

		if (NULL != errorText)
		{
			std::wstring ws(errorText);
			errmes += std::string("\n") + std::string(ws.begin(), ws.end());

			// release memory allocated by FormatMessage()
			LocalFree(errorText);
			errorText = NULL;
		}
	}
	virtual ~exHRESULT(){}
	virtual const char* what() const override
	{
		return errmes.c_str();
	}
	HRESULT GetHR() const
	{
		return res;
	}
private:
	std::string errmes;
	const HRESULT res;
};

class exNoRam : public std::exception
{
public:
	exNoRam()
	{
		errmes = "memory allocation failed - not enough ram?";
	}
	virtual ~exNoRam(){}
	virtual const char* what() const override
	{
		return errmes.c_str();
	}
private:
	std::string errmes;
};

class exMissingFile : public std::exception
{
public:
	exMissingFile(std::wstring filename)
	{
		errmes = std::string(filename.begin(), filename.end());
		errmes = "missing file: " + errmes;
	}
	exMissingFile(std::string filename)
	{
		errmes = "missing file: " + filename;
	}
	virtual ~exMissingFile(){}
	virtual const char* what() const override
	{
		return errmes.c_str();
	}
private:
	std::string errmes;
};

class exInvalidFile : public std::exception
{
public:
	exInvalidFile(std::string filename, std::string reason)
	{
		errmes = "invalid file: | " + filename + std::string(" | reason: ") + reason;
	}
	exInvalidFile(std::string filename)
	{
		errmes = "invalid file: " + filename;
	}
	virtual ~exInvalidFile(){}
	virtual const char* what() const override
	{
		return errmes.c_str();
	}
private:
	std::string errmes;
};