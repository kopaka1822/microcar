#pragma once
#include "ComHandle.h"
#include <math.h>

class Joystick : public ComHandle
{
	struct message
	{
		MSG t;
		char data;
	};



	class ComReader : public Thread
	{
	public:
		ComReader(HANDLE h)
			:
			h(h)
		{}
		virtual DWORD ThreadProc() override
		{
			while (1)
			{
				if (!IsRunning())
					return 0;

				char data;
				DWORD read;
				if (!ReadFile(h, &data, 1, &read, NULL))
				{
					if (GetLastError() == ERROR_IO_PENDING)
						printf("pending error whilst reading!\n");

					SetError();
					return -1;
				}

				//translate
				message msg;
				msg.t = (MSG)(data & MASK_TYPE);
				if ((msg.t == MSG::SPEED) || (msg.t == MSG::STEER))
				{
					msg.data = from6Bit(data);
				}
				else
				{
					msg.data = data & MASK_DATA;
				}

				switch (msg.t)
				{
				case SPEED:
					speed = msg.data;
					break;
				case STEER:
					dir = msg.data;
					break;
				case SPECIAL:
					if (msg.data == MSG::STOP)
						panicButton = true;
					break;
				default:
					return false;
				}
			}
		}
		char GetSpeed() const
		{
			return speed;
		}
		char GetDir() const
		{
			return dir;
		}
		bool GetButton() const
		{
			return panicButton;
		}
	private:
		HANDLE h;
		char dir = 0;
		char speed = 0;
		bool panicButton = false;
	};
public:
	Joystick(HANDLE port)
		:
		ComHandle(port),
		isConnected(false),
		dir(0),
		speed(0),
		reader(port)
	{
		//send init code
		printf("sending init code %X\n", MSG::JOY_INIT);
		if (SendByte(MSG::JOY_INIT))
			printf("sent init code succesfully\n");
		else
			return;
		
		unsigned char tst = 0;
		for(int i = 0; i < 10; i++)
		{
			if (ReadByte(&tst))
			{
				if (tst == MSG::JOY_INIT)
					break;
				else
				{
					if (tst == 0xFF)
						i--;
					else
						printf("wrong response %X\n", tst);
					
					continue;
				}
			}

			printf(".");
			Sleep(1000);
		}
		if (tst == MSG::JOY_INIT)
		{
			isConnected = true;
			reader.Begin();
		}
		else
			printf("wrong response %X\n", tst);
	}
	~Joystick()
	{
		reader.End();
	}
	bool Connected() const
	{
		return isConnected;
	}
	bool Update()
	{
		bool bChange = false;
		if (reader.GetSpeed() != speed)
		{
			bChange = true;
			speed = reader.GetSpeed();
		}

		if (reader.GetDir() != dir)
		{
			bChange = true;
			dir = reader.GetDir();
		}

		if (reader.GetButton() != panicButton)
		{
			bChange = true;
			panicButton = reader.GetButton();
		}

		return bChange;
	}
	int GetSpeed() const
	{
		return speed;
	}
	int GetDir() const
	{
		return dir;
	}
	bool GetPanicButton()
	{
		bool r = panicButton;
		panicButton = false;
		return r;
	}
private:
	bool isConnected;
	char dir;
	char speed;
	bool panicButton = false;

	ComReader reader;
};