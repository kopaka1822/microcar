#pragma once
#include "Graphic.h"
#include <assert.h>
#include "tools.h"

class Font
{
public:
	Font(int fontHei, LPDIRECT3DDEVICE9 pDevice)
		:
		fontH(fontHei),
		col(1.0f,1.0f,1.0f,1.0f)
	{
		UINT weight = FW_MEDIUM;
		HRESULT hr = D3DXCreateFont(
			pDevice, fontH, 0,
			weight, 0, FALSE,
			DEFAULT_CHARSET,
			OUT_DEFAULT_PRECIS,
			DEFAULT_QUALITY,
			DEFAULT_PITCH | FF_DONTCARE,
			TEXT("Consolas"), &pFont);

		assert(!FAILED(hr));

		TEXTMETRICA pTextM;
		pFont->GetTextMetricsA(&pTextM);
		fontWi = pTextM.tmAveCharWidth;
	}
	void Dispose()
	{
		SafeRelease(pFont);
	}
	~Font()
	{
		Dispose();
	}
	void SetColor(D3DXCOLOR c)
	{
		col = c;
	}
	PointI GetMetrics() const
	{
		return PointI(fontWi, fontH);
	}
	void Text(const std::string& t, PointI pos)
	{
		RECT dstRect;
		SetRect(&dstRect, pos.x, pos.y, 0, 0);

		pFont->DrawTextA(NULL, t.c_str(),
			-1, &dstRect, DT_NOCLIP, col);
	}
private:
	const int fontH;
	int fontWi;
	LPD3DXFONT pFont = nullptr;
	D3DXCOLOR col;
};