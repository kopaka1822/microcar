#pragma once
#include "3DCube.h"
#include <vector>
#include "3DTile.h"

class Tilemap
{
public:
	Tilemap(LPDIRECT3DDEVICE9 pDevice, int resolution)
		:
		res(resolution)
	{
		pCube = new Tile3D(pDevice, D3DCOLOR_XRGB(255, 255, 255));
		pTile = new Tile3D(pDevice, D3DCOLOR_XRGB(60, 60, 60));

		blocks.assign(resolution * resolution, false);
	}
	~Tilemap()
	{
		SafeDelete(pCube);
	}
	void Draw(LPDIRECT3DDEVICE9 pDevice,const D3DXMATRIX* trans, PointI mid)
	{
		static const int view = 60;
		const int xStart = max(mid.x - view, 0);
		const int yStart = max(mid.y - view, 0);
		const int xEnd = min(mid.x + view, res);
		const int yEnd = min(mid.y + view, res);

		if (xStart >= xEnd || yStart >= yEnd)
			return;

		D3DXMATRIX matTrans;
		for (int x = xStart; x < xEnd; x++)
		{
			for (int y = yStart; y < yEnd; y++)
			{
				if (blocks[y * res + x])
				{
					D3DXMatrixTranslation(&matTrans, float(x), 0.0f, float(y));
					if (trans)
						matTrans = matTrans * (*trans);

					pDevice->SetTransform(D3DTS_WORLD, &matTrans);
					pCube->Draw(pDevice);
				}
				else if (((y % 2) + (x % 2)) == 2)
				{
					D3DXMatrixTranslation(&matTrans, float(x), 0.0f, float(y));
					if (trans)
						matTrans = matTrans * (*trans);

					pDevice->SetTransform(D3DTS_WORLD, &matTrans);
					pTile->Draw(pDevice);
				}
			}
		}
	}

	//returns false if not in map
	bool SetBlock(const PointI& p, bool t)
	{
		if (p.x < res && p.x >= 0)
			if (p.y < res && p.y >= 0)
			{
				blocks[p.y * res + p.x] = t;
				return true;
			}
		return false;
	}
	void Clear(bool val)
	{
		blocks.assign(res * res, val);
	}
private:
	Tile3D* pCube = nullptr;

	Tile3D* pTile = nullptr;
	const int res;

	std::vector<bool> blocks;
};