#pragma once
#include "Graphic.h"
#include "tools.h"

class Cube3D
{
	struct Vertex
	{
		float x, y, z;
		DWORD color;

		enum FVF
		{
			FVF_Flags = D3DFVF_XYZ | D3DFVF_DIFFUSE
		};
	};
public:
	Cube3D(LPDIRECT3DDEVICE9 pDevice, DWORD col = 0xFFFFFFFF)
	{
		Vertex points[8];
		points[0] = { 0.0f, 1.0f, 0.0f, col }; // 0
		points[1] = { 1.0f, 1.0f, 0.0f, col }; // 1
		points[2] = { 0.0f, 0.0f, 0.0f, col }; // 2
		points[3] = { 1.0f, 0.0f, 0.0f, col }; // 3
		points[4] = { 0.0f, 1.0f, 1.0f, col }; // 4
		points[5] = { 0.0f, 0.0f, 1.0f, col }; // 5
		points[6] = { 1.0f, 1.0f, 1.0f, col }; // 6
		points[7] = { 1.0f, 0.0f, 1.0f, col };  // 7

		WORD g_cubeIndices[] =
		{
			0, 1, 2, 3, // Quad 0
			4, 5, 6, 7, // Quad 1
			4, 6, 0, 1, // Quad 2
			5, 2, 7, 3, // Quad 3
			1, 6, 3, 7, // Quad 4
			0, 2, 4, 5  // Quad 5
		};




		pDevice->CreateVertexBuffer(8 * sizeof(Vertex),
			D3DUSAGE_WRITEONLY, Vertex::FVF_Flags,
			D3DPOOL_DEFAULT,
			&pVertexBuffer_Indexed,
			NULL);

		void* pVertices;
		pVertexBuffer_Indexed->Lock(0, sizeof(points), (void**)&pVertices, 0);
		memcpy(pVertices, points, sizeof(points));
		pVertexBuffer_Indexed->Unlock();

		//
		// Create an index buffer to use with our indexed vertex buffer...
		//

		pDevice->CreateIndexBuffer(24 * sizeof(WORD),
			D3DUSAGE_WRITEONLY,
			D3DFMT_INDEX16,
			D3DPOOL_DEFAULT,
			&pIndexBuffer,
			NULL);
		WORD *pIndices = NULL;

		pIndexBuffer->Lock(0, sizeof(g_cubeIndices), (void**)&pIndices, 0);
		memcpy(pIndices, g_cubeIndices, sizeof(g_cubeIndices));
		pIndexBuffer->Unlock();
	}
	void Draw(LPDIRECT3DDEVICE9 pDevice)
	{
		pDevice->SetStreamSource(0, pVertexBuffer_Indexed, 0, sizeof(Vertex));
		pDevice->SetIndices(pIndexBuffer);
		pDevice->SetFVF(Vertex::FVF_Flags);

		pDevice->DrawIndexedPrimitive(D3DPT_TRIANGLESTRIP, 0, 0, 8, 0, 2);
		pDevice->DrawIndexedPrimitive(D3DPT_TRIANGLESTRIP, 0, 0, 8, 4, 2);
		pDevice->DrawIndexedPrimitive(D3DPT_TRIANGLESTRIP, 0, 0, 8, 8, 2);
		pDevice->DrawIndexedPrimitive(D3DPT_TRIANGLESTRIP, 0, 0, 8, 12, 2);
		pDevice->DrawIndexedPrimitive(D3DPT_TRIANGLESTRIP, 0, 0, 8, 16, 2);
		pDevice->DrawIndexedPrimitive(D3DPT_TRIANGLESTRIP, 0, 0, 8, 20, 2);
	}
	void Dispose()
	{
		SafeRelease(pVertexBuffer_Indexed);
		SafeRelease(pIndexBuffer);
	}
	~Cube3D()
	{
		Dispose();
	}
private:
	LPDIRECT3DVERTEXBUFFER9 pVertexBuffer_Indexed = NULL;
	LPDIRECT3DINDEXBUFFER9  pIndexBuffer = NULL;
};